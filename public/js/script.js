$(document).ready(function(){
    $('#orderForm').on('submit',function(e)
    {
        e.preventDefault();
        var tennguoinhan=$('#tennguoinhan').val();
        var diachi=$('#diachi').val();
        var sdt=$('#sdt').val();
        var order={};
        var dataArray=new Array();
        order.tennguoinhan=tennguoinhan;
        order.diachi=diachi;
        order.sdt=sdt;
        order.tongtien=simpleCart.grandTotal();
        dataArray.push(order);
        var count=0;
        simpleCart.each(function(item,x){
            var itemContent={};
            var itemDes={};
            if(item.get('type')!=0)
            {
                itemContent.product_id=item.get('id');
                itemContent.qty=item.get('quantity');
                dataArray.push(itemContent);
                count ++;
            }
            else{
                itemDes.front=item.get('imagepath');
                itemDes.back=item.get('backview');
                itemDes.price=item.get('price');
                itemDes.qty=item.get('qty');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                });
                $.ajax({
                    url:'/products',
                    type:'POST',
                    dataType:'json',
                    data:{design:itemDes},
                    async:true,
                    success:function(data){
                        count ++;
                        itemContent.product_id=data;
                        itemContent.qty=item.get('quantity');
                        dataArray.push(itemContent);
                        if(count == simpleCart.items().length) {
                            saveOrder(dataArray);
                        }
                    }
                })
            } 
            console.log(count);
        })
    })
    $('#btnOrder').on('click',function(){
        $('#orderForm').submit();
    });
    $('#btn-add-item').on('click',function(){
        var id=$("#idsp").attr('value');
        var name=$('#tensp').text();
        var price=parseInt($('#price').text());
        var qty=$('#quantity').val();
        var imagepath=$('#img-1').attr('src');
        var backview=$('#img-2').attr('src');
        simpleCart.add({
            id:id,
            name:name,
            price:price,
            quantity:qty,
            imagepath:imagepath,
            backview:backview,
            type:1,
        })
    })
})
function saveOrder(data){
    console.log(data);
    $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });
    $.ajax({
        url: '/cart',
        type: 'POST',
        dataType: "json",
        data: {myData:data},
        success: function () {
            simpleCart.empty();
            toastr.success('Đặt hàng thành công!');
        },
    })
}
