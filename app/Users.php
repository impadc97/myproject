<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
class Users extends \Eloquent implements Authenticatable 
{
    public $remember_token=false;
    use AuthenticableTrait;
    //
    public function Orders(){
        return $this->hasMany('App/Orders','user_id');
    }
    protected $table  = 'Users';
    protected $fillable = ['email', 'password','tenuser','diachi','sdt'];
    protected $hidden=['password'];
}
