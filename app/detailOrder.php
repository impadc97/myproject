<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detailOrder extends Model
{
    //
    protected $fillable = ['product_id', 'qty'];
    public function order()
    {
        return $this->belongsTo('App\Orders','order_id');
    }
    public function product()
    {
        return $this->belongsTo('App\Products','product_id');
    }
}
