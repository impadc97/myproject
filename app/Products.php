<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    public function listimage()
    {
        return $this->hasMany('App\listImage','product_id');
    }
    public function detail()
    {
        return $this->hasMany('App\detailOrder','product_id');
    }
    public function totalOrder(){
        return $this->hasOne('App\detailOrder','product_id')
        ->selectRaw('product_id ,sum(qty) as aggregate')
        ->groupBy('product_id');
    }
    public function getTotalOrder(){
        if(!array_key_exists('totalOrder',$this->relations)){
            $this->load('totalOrder');
        }
        $relation=$this->getRelation('totalOrder');
        return ($relation)?$relation->aggregate : null;
    }
}
