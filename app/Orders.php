<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    //
    public function user(){
        return $this->belongsTo('App\Users','user_id');
    }
    public function detail(){
        return $this->hasMany('App\detailOrder','order_id');
    }
}
