<?php

namespace App\Http\Controllers;
use Auth;
use App\Products;
use Illuminate\Http\Request;
use Debugbar;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get all products
        $allProducts=Products::where('type',1)->get()->toArray();
        return view('welcome')->with('allProducts',$allProducts);
    }
    public function list()
    {
        return view('admin/products');
    }
    public function json(){
        $page = isset($_GET['page']) ? intval($_GET['page']) :1;
        $rows = isset($_GET['rows']) ? intval($_GET['rows']) :10;
        $sort= isset($_GET['sort']) ?strval($_GET['sort']):'id';
        $order = isset($_GET['order']) ? strval($_GET['order']) : 'asc';
        $offset=($page-1)*$rows;
        $tmp=Products::where('type',1)->with('totalOrder')->orderBy($sort,$order)->skip($offset)->take($rows)->get();
        $result['total']=Products::all()->count();
        $result['rows']=$tmp;
        return json_encode($result);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDes()
    {
        $req=$_POST['design'];
        $datafront=$req['front'];
        $databack=$req['back'];
        list($type, $datafront) = explode(';', $datafront);
        list(, $datafront)      = explode(',', $datafront);
        list($type, $databack) = explode(';', $databack);
        list(, $databack)      = explode(',', $databack);
        $datafront=base64_decode($datafront);
        $databack=base64_decode($databack);
        $namefront=md5(rand()*time());
        $nameback=md5(rand()*time());
        file_put_contents('design/'.$namefront.'.jpg',$datafront);
        file_put_contents('design/'.$nameback.'.jpg',$databack);
        $objProducts=new Products();
        $objProducts->tensp="Áo tự thiết kế_".Auth::user()->id;
        $objProducts->image='/design/'.$namefront.'.jpg';
        $objProducts->image1='/design/'.$nameback.'.jpg';
        $objProducts->giatien=$req['price'];
        $objProducts->qty=1;
        $objProducts->type=0;
        $objProducts->save();
        return $objProducts->id;
    }
    public function store(Request $request)
    {
        //
        if(isset($_POST) && isset($_FILES['file'])){
            $objProducts=new Products();
            if(move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$_FILES['file']['name'])){
                $objProducts->image='/uploads/'.$_FILES['file']['name'];
            }
            $tmp=json_decode($_POST['data'],true);
            $objProducts->tensp=$tmp[0]['tensp'];
            $objProducts->giatien=$tmp[0]['giatien'];
            $objProducts->qty=$tmp[0]['qty'];
            $objProducts->save();
        }
        return json_encode(array('success'=>true));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objProducts=Products::find($id)->toArray();
        if($objProducts['type']==1)
        return view('product')->with('product',$objProducts);
        else return abort(404,'Page not found!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $objProducts=Products::find($id);
        if(isset($_POST)){
            $tmp=json_decode($_POST['data'],true);
            $objProducts->tensp=$tmp[0]['tensp'];
            $objProducts->giatien=$tmp[0]['giatien'];
            $objProducts->qty=$tmp[0]['qty'];
            $objProducts->save();
        }
        if(isset($_FILES['file'])){
            if(move_uploaded_file($_FILES['file']['tmp_name'],'uploads/'.$_FILES['file']['name'])){
                $objProducts->image='/uploads/'.$_FILES['file']['name'];
            }
        }
        $objProducts->save();
        return json_encode(array('success'=>true));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
        $id = intval($_POST['id']);
        $objProducts=Products::find($id);
        $objProducts->delete();
        return json_encode(array('success'=>true));
    }
}
