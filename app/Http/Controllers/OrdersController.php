<?php

namespace App\Http\Controllers;
use Debugbar;
use Auth;
use App\Orders;
use App\Products;
use App\detailOrder;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isAdmin)
        {
            $objOrders=new Orders();
            $allOrders=$objOrders->all()->toArray();
            return view('admin/orders')->with('allOrders',$allOrders);
        }
        else return redirect()->route('home');
    }
    public function json(){
        $page = isset($_GET['page']) ? intval($_GET['page']) :1;
        $rows = isset($_GET['rows']) ? intval($_GET['rows']) :10;
        $sort=isset($_GET['sort']) ?strval($_GET['sort']):'id';
        $order = isset($_GET['order']) ? strval($_GET['order']) : 'asc';
        $offset=($page-1)*$rows;
        $tmp=new Orders();
        $objOrders=$tmp->orderBy($sort,$order)->skip($offset)->take($rows)->get();
        $result['total']=Orders::all()->count();
        $result['rows']=$objOrders;
        return json_encode($result);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $req=$_POST['myData'];
        Debugbar::info($req);
        $tennguoinhan=$req[0]['tennguoinhan'];
        $diachi=$req[0]['diachi'];
        $sdt=$req[0]['sdt'];
        $tongtien=$req[0]['tongtien'];
        $user_id=Auth::user()->id;
        $objOrders=new Orders();
        $objOrders->user_id=$user_id;
        $objOrders->diachi=$diachi;
        $objOrders->tennguoinhan=$tennguoinhan;
        $objOrders->sdt=$sdt;
        $objOrders->tongtien=$tongtien;
        $objOrders->save();
        for($i=1;$i<count($req);$i++)
        {
            $objOrders->detail()->create([
                'product_id'=>$req[$i]['product_id'],
                'qty'=>$req[$i]['qty'],
            ]);
        }
        return json_encode(array('success'=>true));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $objOrders=detailOrder::with('product')->where('order_id',$id)->get()->toArray();
        app('debugbar')->info($objOrders);
        return view ('detail')->with('orders',$objOrders);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(Orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $req=$_POST['myData'];
        $objOrder = Orders::find($req['id']);
        $objOrder->tennguoinhan=$req['tennguoinhan'];
        $objOrder->sdt=$req['sdt'];
        $objOrder->diachi=$req['diachi'];
        $objOrder->save(); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
        $id=intval($_POST['id']);
        $objOrder = Orders::find($id);
        $objOrder->delete();
        return json_encode(array('success'=>true));
    }
    public function history(){
        $user_id=Auth::user()->id;
        $objOrders = Orders::where('user_id',$user_id)->get()->toArray();
        app('debugbar')->info($objOrders);
        return view('history')->with('orders',$objOrders);
    }
}
