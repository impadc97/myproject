<?php

namespace App\Http\Controllers;
use App\lib_img;
use Illuminate\Http\Request;
use Debugbar;
class LibImgController extends Controller
{
    //
    public function index()
    {
        $objLib=new lib_img();
        $allLib=$objLib->all()->toArray();
        return view('design')->with('allImage',$allLib);
    }
}
