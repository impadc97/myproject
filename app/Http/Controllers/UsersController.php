<?php

namespace App\Http\Controllers;
use Auth;
use Mail;
use App\Users;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Mail\UserActivationEmail;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->isAdmin)
        {
            $objUser=new Users();
            $allUsers=$objUser->all()->toArray();
            return view('admin/users')->with('allUsers',$allUsers);
        }
        else return redirect()->route('home');
    }
    public function json(){
        $page = isset($_GET['page']) ? intval($_GET['page']) :1;
        $rows = isset($_GET['rows']) ? intval($_GET['rows']) :10;
        $sort=isset($_GET['sort']) ?strval($_GET['sort']):'id';
        $order = isset($_GET['order']) ? strval($_GET['order']) : 'asc';
        $offset=($page-1)*$rows;
        $tmp=new Users();
        $objUsers=$tmp->orderBy($sort,$order)->skip($offset)->take($rows)->get();
        $result['total']=Users::all()->count();
        $result['rows']=$objUsers;
        return json_encode($result);
    }
    public function ban(){
        $id=$_POST['id'];
        $objUser=Users::find($id);
        $objUser->isActive=!$objUser->isActive;
        $objUser->save();
        return json_encode(array('success'=>true));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('auth\register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(Users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit(Users $users)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function contact(Request $request){
        $req=$request->all();
        $data['message']=$req['message'];
        $data['email']=$req['email'];
        $data['name']=$req['name'];
        $mailable=new UserActivationEmail($data);
        Mail::to('nhanngoc1997@gmail.com')->send($mailable);
        return json_encode(array('success'=>true));
    }
    public function update(Request $request)
    {
        //
        $req=$request->all();
        $email=$req['email'];
        $tenuser=$req['hoten'];
        $diachi=$req['diachi'];
        $sdt=$req['sdt'];
        $id=$req['id'];
        $objUser=new Users();
        $user=$objUser->find($id);
        $user->email=$email;
        $user->tenuser=$tenuser;
        $user->diachi=$diachi;
        $user->sdt=$sdt;
        $user->save();
        return redirect()->to('home'); 
    }
    public function changePwdRules(array $data)
    {
        return Validator::make($data, [
            'newpwd' => 'required|string|min:6|confirmed',
            'curpwd'=>'required|string|min:6',
        ]);
    }
    public function changePwd(Request $request)
    {
        if(Auth::check())
        {
            $req=$request->all();
            $validator=$this->changePwdRules($req);
            if($validator->fails())
            {
                return response()->json(array('error' => $validator->getMessageBag()->toArray()), 400);
            }
            else
            { 
                $current_password = Auth::user()->password;           
                if(Hash::check($req['curpwd'], $current_password))
                {           
                    $user_id = Auth::user()->id;                       
                    $obj_user = UserS::find($user_id);
                    $obj_user->password = bcrypt($req['newpwd']);;
                    $obj_user->save(); 
                    return "ok";
                }
                else
                {           
                    $error = array('current-password' => 'Please enter correct current password');
                    return response()->json(array('error' => $error), 400);   
                }
            }
        }
        else{
            return redirect()->to('/'); 
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy(Users $users)
    {
        //
        $id=$_POST['id'];
        $objUser=Users::find($id);
        $objUser->delete();
        return json_encode(array('success'=>true));
    }
}
