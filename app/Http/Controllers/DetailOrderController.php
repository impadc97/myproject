<?php

namespace App\Http\Controllers;

use App\detailOrder;
use Illuminate\Http\Request;

class DetailOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\detailOrder  $detailOrder
     * @return \Illuminate\Http\Response
     */
    public function show(detailOrder $detailOrder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\detailOrder  $detailOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(detailOrder $detailOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\detailOrder  $detailOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, detailOrder $detailOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\detailOrder  $detailOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(detailOrder $detailOrder)
    {
        //
    }
}
