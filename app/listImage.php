<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class listImage extends Model
{
    //
    public function products(){
        return $this->belongsTo('App/Products','product_id');
    }
}
