@extends('layouts.app')
@section('content')
<nav class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">My Profile</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="/home">Hồ sơ tài khoản<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
          <li  class="active"><a href="home/history">Lịch sử đặt hàng<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-file"></span></a></li>
          <li><a href="/home/changepwd">Đổi mật khẩu<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a></li>
        </ul>
      </div>
    </div>
</nav>
  <div class="container">
    <div class="row">
        <div class="col-md-9 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <h2>Lịch sử đặt hàng</h2>
                    <table border="1px" id="userstable"style="width:100%">
                                <tr>
                                    <th>Mã đơn hàng</th>
                                    <th>Giá tiền</th>
                                    <th>Họ tên người nhận</th>
                                    <th>SĐT người nhận</th>
                                    <th>Địa chỉ giao hàng</th>
                                    <th></th>
                                </tr>
                                @foreach ($orders as $Orders)
                                <tr>
                                    <td>{{$Orders['id']}}</td>
                                    <td>{{$Orders['tongtien']}}</td>
                                    <td>{{$Orders['tennguoinhan']}}</td>
                                    <td>{{$Orders['sdt']}}</td>
                                    <td>{{$Orders['diachi']}}</td>
                                    <td><a href="history/{{$Orders['id']}}">Chi tiết đơn hàng</a></td>
                                </tr>
                                @endforeach
                            </table>
                </div>
        </div>
    </div>
</div>


@endsection