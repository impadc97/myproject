@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 gutter">
        <table id="usertable" class="easyui-datagrid" url="/admin/usersjson" idField='id' method="get" 
        toolbar ="#toolbar" singleSelect="true" fitColumns="true" pagination="true" 
        data-options="onSelectPage:function(pageNumber,pageSize){
             $('#usertable').datagrid('refresh','/admin/usersjson/?page='+pageNumber);}">
            <thead>
                <tr>
                    <th field="id" width="50" sortable="true">ID</th>
                    <th field="email" width="200">Email</th>
                    <th field="tenuser" width="200" sortable="true">Họ tên</th>
                    <th field="diachi" width="200">Địa chỉ</th>
                    <th field="sdt" width="150">Số điện thoại</th>
                    <th field="isActive" width="50" sortable="true">Status</th>
                </tr>
            </thead>
        </table>
        <div id="toolbar">
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyUser()">Destroy</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-no" plain="true" onclick="banUser()">Ban/Unban</a>
        </div>
    </div>
</div>
<script>
    $('#usertable').edatagrid();
    function banUser(){
        var row=$('#usertable').datagrid('getSelected');
        if(row){
            $.ajax({
                url:'usersjson/ban',
                type:'POST',
                data:{id:row.id},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success:function(){
                    $('#usertable').datagrid('reload');
                },
            });
        }
    }
    function destroyUser(){
        var row=$('#usertable').datagrid('getSelected');
        if(row){
            $.messager.confirm('Confirm','Are you sure you want to destroy this user?',function(r){
                if(r){
                    $.ajax({
                        url:'usersjson/del',
                        type:'POST',
                        data:{id:row.id},
                        dataType:'json',
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        success:function(){
                            $('#usertable').datagrid('reload');
                        },
                    })
                }
            })
        }
    }
</script>
@endsection