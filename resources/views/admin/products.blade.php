@extends('layouts.admin')
@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 ">
        <table id="prodtable" class="easyui-datagrid" singleSelect=true fitColumns=true toolbar='#tb'data-options="onSelectPage:function(pageNumber,pageSize){
    $('#prodtable').datagrid('refresh','/admin/productsjson/?page='+pageNumber);}">
        </table>
        <div id="tb">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="addProduct()">Add</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-edit" plain="true" onclick="editProduct()">Edit</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="deleteProduct()">Delete</a>
        </div>
    </div>
</div>
<div id="dlg" class="easyui-dialog" style="width:400px;height:350px;padding:10px 20px"
			closed="true" buttons="#dlg-buttons">
		<div class="ftitle">Thông tin sản phẩm</div>
		<form id="fm" method="post" enctype="multipart/form-data" action="" novalidate>
			<div class="fitem">
				<label>Tên sản phẩm:</label>
				<input id="tensp" name="tensp" style="width:100%" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label>Giá tiền:</label>
				<input id="giatien" name="giatien" style="width:100%" class="easyui-textbox" required="true">
			</div>
			<div class="fitem">
				<label>Ảnh:</label>
				<input name="image" id="image" type="file" style="width:100%">
			</div>
			<div class="fitem">
				<label>Số lượng:</label>
				<input id="qty" name="qty" style="width:100%" class="easyui-numberbox" validType="number">
			</div>
		</form>
</div>
	<div id="dlg-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton c6" iconCls="icon-ok" onclick="saveProduct()" style="width:90px">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')" style="width:90px">Cancel</a>
    </div>
<script>
  $(document).ready(function() 
  { 
      var url;
    $('#prodtable').datagrid({
    url:'/admin/productsjson',
    method:'get',
    columns:[[
        {field:"id",sortable:"true", maxwidth:"50",title:'ID'},
        {field:"tensp",sortable:"true", maxwidth:"200", title:'Tên sản phẩm'},
        {field:"image",maxwidth:"400",title:'Ảnh',formatter:function(value,row){return '<img style="width:80px;height:80px"; src="'+row.image+'"/>'}},
        {field:"giatien",sortable:"true", maxwidth:"150",title:'Giá tiền'},
        {field:"qty",sortable:"true", maxwidth:"80",title:'Số lượng'},
        {field:"total_order.aggregate", maxwidth:"100",title:'Số lượng đặt hàng',formatter:function(value,row)
            {if(row.total_order!=null) return row.total_order.aggregate; else return 0;}},
    ]],
    idField:'id',
    toolbar:'#toolbar',
    pagination:'true',
    
    });
  });
  function deleteProduct(){
      var row=$('#prodtable').datagrid('getSelected');
      if(row){
        $.messager.confirm('Confirm','Are you sure you want to destroy this product?',function(r){
        if (r){
                $.ajax({
                    url:'productsjson/del',
                    type:'POST',
                    data:{id:row.id},
                    dataType: 'json',
                    headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    success:function(){
                        $('#prodtable').datagrid('reload');
                    },
                });
            }
        });
      }
  }
  function addProduct(){
		$('#dlg').dialog('open').dialog('setTitle','New Product');
        $('#fm').form('clear');
        url='/admin/productsjson/add';
    }
    function saveProduct(){
        $('#fm').submit();
    }
    function editProduct(){
        var row=$('#prodtable').datagrid('getSelected');
        if(row)
        {
            delete row['image'];
            $('#dlg').dialog('open').dialog('setTitle','Edit Product');
            $('#fm').form('load',row);
            url='/admin/productsjson/edit/'+row.id;
        }
    }
    $('#fm').on('submit',function(e){
        e.preventDefault();
        var file_data=$('#image').prop('files')[0];
        if(file_data!=null) var type=file_data.type;
        else type=0;
        var match= ["image/gif","image/png","image/jpg","image/jpeg"];
        if(type == match[0] || type == match[1] || type == match[2] || type==match[3] || type==0)
        {
            var form_data = new FormData();
            form_data.append('file', file_data);
            var other_data=[];
            var tensp=$('#tensp').val();
            var giatien=$('#giatien').val();
            var qty=$('#qty').val();
            other_data.push({'tensp':tensp,'giatien':giatien,'qty':qty});
            form_data.append('data',JSON.stringify(other_data));
            $.ajax({
                url:url,
                type:'POST',
                data:form_data,
                processData: false,
                contentType: false,
                dataType: 'text',
                headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success:function(){
                    $('#dlg').dialog('close');
                    $('#prodtable').datagrid('reload');
                },
            });
        }
    });

</script>
@endsection