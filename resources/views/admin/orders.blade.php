@extends('layouts.admin')
@section('content')
<div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 gutter">
              <table id="ordertable" class="easyui-datagrid" url="/admin/ordersjson" idField='id' method="get" toolbar ="#toolbar"
                 singleSelect="true" fitColumns="true" pagination="true" data-options="onSelectPage:function(pageNumber,pageSize){
                $('#ordertable').datagrid('refresh','/admin/ordersjson/?page='+pageNumber);}">
                  <thead frozen="true">
                  <tr>
                  <th field="id" width="50">ID</th>
                  <th field="user_id" width="100">User ID</th>
                  <th field="tongtien" sortable="true" width="150">Giá tiền</th>
                  <th field="tennguoinhan" editor="{type:'validatebox',options:{required:true}}" width="200">Họ tên người nhận</th>
                  <th field="sdt" editor="{type:'validatebox',options:{required:true}}" width="150">SĐT người nhận</th>
                  <th field="diachi" editor="{type:'validatebox',options:{required:true}}" width="200">Địa chỉ giao hàng</th>
                  <th field="created_at" width="150" sortable="true">Ngày đặt hàng</th>
                </tr>
                </thead>
              </table>
              <div id="toolbar">
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="destroyOrder()">Destroy</a>
                <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveOrder()">Save</a>
                <a href="#" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#ordertable').edatagrid('cancelRow')">Cancel</a>
              </div>
            </div>
</div>
<script>
    $('#ordertable').edatagrid();
    function destroyOrder(){
        var row=$('#ordertable').datagrid('getSelected');
        if(row)
        {
            $.messager.confirm('Confirm','Are you sure you want to destroy this order?',function(r){
                if (r){
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: 'ordersjson/delete',
                        type: 'POST',
                        dataType: "json",
                        data: {id:row.id},
                        success: function () {
                            $('#ordertable').datagrid('reload');
                        },
                    })
                }
            });
        }
    }
    function saveOrder(){
        var row=$('#ordertable').datagrid('getSelected'); //lay dong dang chon ( lay theo id)
        var rowIndex = $('#ordertable').datagrid('getRowIndex', row); //doi sang stt cua dong trong table
        if(row)
        {
            $('#ordertable').datagrid('endEdit',rowIndex); // ket thuc event edit
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                $.ajax({ // post update
                    url: 'ordersjson/update',
                    type: 'POST',
                    dataType: "json",
                    data: {myData:row},
                    success: function () {
                        $('#ordertable').datagrid('reload');
                    },
                })
            };
    }

</script>
@endsection
