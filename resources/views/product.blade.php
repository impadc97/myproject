@extends('layouts.lib')
@section('content')
        <link href="{{asset('css/product.css')}}" rel="stylesheet">
		<div class="card">
			<div class="container">
				<div class="wrapper row">
					<div class="preview col-md-6">
						<div class="preview-pic tab-content">
                            <label id="idsp" style="display:none" value="{{$product['id']}}"></label>
						  <div class="tab-pane active" id="pic-1"><img id="img-1" src="{{$product['image']}}" /></div>
						  <div class="tab-pane" id="pic-2"><img id="img-2" src="{{$product['image1']}}" /></div>
						</div>
						<ul class="preview-thumbnail nav nav-tabs" id="imgpreview">
						  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="{{$product['image']}}" /></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="{{$product['image1']}}" /></a></li>
						</ul>
						
					</div>
					<div class="details col-md-6">
						<h3 class="product-title" id="tensp">{{$product['tensp']}}</h3>
						<p class="product-description">Suspendisse quos? Tempus cras iure temporibus? Eu laudantium cubilia sem sem! Repudiandae et! Massa senectus enim minim sociosqu delectus posuere.</p>
						<h4 class="price">Giá hiện tại: <span id="price">{{$product['giatien']}}&#x20ab</span></h4>
						<p class="vote"><strong>91%</strong> of buyers enjoyed this product! <strong>(87 votes)</strong></p>
						<h5 class="sizes">Số lượng:
							<input type="number" min=1 id="quantity" name="quantity">
						</h5>
						<h5 class="colors">Màu sắc:
							<span class="color orange not-available" data-toggle="tooltip" title="Not In store"></span>
							<span class="color green"></span>
							<span class="color blue"></span>
						</h5>
						<div class="action">
							<button class="btn btn-info" id="btn-add-item" type="button">Thêm vào giỏ</button>
						</div>
					</div>
				</div>
			</div>
        </div>
        