@extends('layouts.lib')
@section('content')
        <div class="flex-center position-ref full-height">
            <div id="demo" class="carousel slide" data-ride="carousel">

                <!-- Indicators -->
                <ul class="carousel-indicators">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                    <li data-target="#demo" data-slide-to="2"></li>
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <img src="img/sale-off1.jpg" alt="Los Angeles">
                    </div>
                    <div class="carousel-item">
                    <img src="img/sale-off2.jpg" alt="Chicago">
                    </div>
                    <div class="carousel-item">
                    <img src="img/sale-off3.jpg" alt="New York">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#demo" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>

            </div>
            <div class="banner-top hidden-lg-down">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="banner-thumb">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="thumb">
                                        <i class="fas fa-shipping-fast fa-2x"></i>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <h6>SHIPPING</h6>
                                    <p>Vận chuyển nhanh, miễn phí nội thành,...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="banner-thumb">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="thumb">
                                        <i class="fas fa-check fa-2x"></i>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <h6>CHECK</h6>
                                    <p>Bạn có thể kiểm tra hàng và có thể đổi trả nếu không vừa ý</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="banner-thumb">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="thumb">
                                        <i class="fas fa-dollar-sign fa-2x"></i>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <h6>LOW PRICE</h6>
                                    <p>Chúng tôi đảm bảo tiệm bán đồ rẻ nhất nếu tìm thấy tiệm rẻ hơn sẽ hoàn tiền gấp đôi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <center><h2>Sản phẩm nổi bật</h2></center>
            </div>
            <div class="container-fluid">
                <div class="tab-pane active " id="home" >
                <div class="row">
                    @foreach ($allProducts as $Products)
                    <div class="col-sm-4">
                        <div class="product simpleCart_shelfItem">
                            <label class="item_id" style="display:none">{{$Products['id']}}</label>
                            <center><a href="products/{{$Products['id']}}"><img class="img-responsive item_imagepath" alt="Card image cap" id="card1" src="{{$Products['image']}}" width="200" height="200"></center></a>
                            <div class="product-retail">
                                <span>
                                    <center><h5 class="item_name">{{$Products['tensp']}}</h5></center>
                                    <label class="item_type" style="display:none">1</label>
                                </span>
                                <center><p class="item_price" style="color:brown">{{$Products['giatien']}}đ
                                <button class="item_add btn btn-primary" href="javascript:;"> Thêm </button></p></center>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                </div>
            </div>
         </div>
         
@endsection
