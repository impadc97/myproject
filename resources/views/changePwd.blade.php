
@extends('layouts.app')

@section('content')
<nav class="navbar navbar-default sidebar" role="navigation">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">My Profile</a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="../home">Hồ sơ tài khoản<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-user"></span></a></li>
          <li><a href="history">Lịch sử đặt hàng<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-file"></span></a></li>
          <li class="active"><a href="#">Đổi mật khẩu<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-cog"></span></a></li>
        </ul>
      </div>
    </div>
  </nav>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form class="form-horizontal" action="{{URL::action('UsersController@changePwd')}}" method='POST'>
                    <input type="hidden" name="_method" value="PUT">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <input type="hidden" name="id" value="{{Auth::user()->id}}">
                        <h2>Đổi mật khẩu</h2>
                        <hr>
                        <div class="form-group">
                            <label for="curpwd" class="col-md-3 align-right">Mật khẩu hiện tại:</label>
                            <div class="col-md-6">
                                <input class="form-control validate" type="password" id="curpwd" data-type="text" name="curpwd"required> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpwd" class="col-md-3 align-right">Mật khẩu mới:</label>
                            <div class="col-md-6">
                                <input class="form-control validate" id="newpwd" type="password" data-type="text" name="newpwd" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="newpwd_confirm"class="col-md-3 align-right">Nhập lại mật khẩu:</label>
                            <div class="col-md-6">
                                <input class="form-control validate" id="newpwd_confirm" type="password" data-type="text" name="newpwd_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 align-right"></label>
                            <button class="col-md-2 btn btn-primary" type="submit">Lưu</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
