@extends('layouts.design')
@section('content')
<div class="preview" style="margin-left:5%;" data-spy="scroll" data-target=".subnav" data-offset="80">
    <div class="page-header">
                <h1>Customize T-Shirt</h1>
    </div>
    <div class ="row">
        <div class="span3">
		    	<div class="tabbable"> <!-- Only required for left/right tabs -->
				  <ul class="nav nav-tabs">
				  	<li class="active"><a href="#tab1" data-toggle="tab">Màu áo</a></li>				    
				    <li><a href="#tab2" data-toggle="tab">Hoạ tiết</a></li>
				  </ul>
				  <div class="tab-content">
				     <div class="tab-pane active" id="tab1">
					    <div class="well">
							<ul class="list-inline">
								<li class="color-preview" title="White" style="background-color:#ffffff;"></li>
								<li class="color-preview" title="Dark Heather" style="background-color:#616161;"></li>
								<li class="color-preview" title="Gray" style="background-color:#f0f0f0;"></li>
								<li class="color-preview" title="Charcoal" style="background-color:#5b5b5b;"></li>
								<li class="color-preview" title="Black" style="background-color:#222222;"></li>
								<li class="color-preview" title="Heather Orange" style="background-color:#fc8d74;"></li>
								<li class="color-preview" title="Heather Dark Chocolate" style="background-color:#432d26;"></li>
								<li class="color-preview" title="Salmon" style="background-color:#eead91;"></li>
								<li class="color-preview" title="Chesnut" style="background-color:#806355;"></li>
								<li class="color-preview" title="Dark Chocolate" style="background-color:#382d21;"></li>
								<li class="color-preview" title="Citrus Yellow" style="background-color:#faef93;"></li>
								<li class="color-preview" title="Avocado" style="background-color:#aeba5e;"></li>
								<li class="color-preview" title="Kiwi" style="background-color:#8aa140;"></li>
								<li class="color-preview" title="Irish Green" style="background-color:#1f6522;"></li>
								<li class="color-preview" title="Scrub Green" style="background-color:#13afa2;"></li>
								<li class="color-preview" title="Teal Ice" style="background-color:#b8d5d7;"></li>
								<li class="color-preview" title="Heather Sapphire" style="background-color:#15aeda;"></li>
								<li class="color-preview" title="Sky" style="background-color:#a5def8;"></li>
								<li class="color-preview" title="Antique Sapphire" style="background-color:#0f77c0;"></li>
								<li class="color-preview" title="Heather Navy" style="background-color:#3469b7;"></li>							
								<li class="color-preview" title="Cherry Red" style="background-color:#c50404;"></li>
							</ul>
						</div>			      
				     </div>				   
				    <div class="tab-pane" id="tab2">
				    	<div class="well">
				    		<div class="input-append">
                              <input class="span2" id="text-string" type="text" placeholder="add text here...">
                              <button id ="add-text" type="button" class="btn btn-default btn-sm">
                                    <span class="glyphicon glyphicon-chevron-right"></span> 
                                  </button>
							  <hr>
							</div>
							<center><a id="uploadImg" class="btn btn-info">
							<span class="fa fa-download"></span> Upload ảnh</a></center>
							<input type="file" id="inputFile" style="display:none">
								<hr>
							<div id="avatarlist">
								@foreach($allImage as $image)
                                <img style="cursor:pointer;width:100px;height:100px;" crossorigin="anonymous" class="img-polaroid" src="{{$image['image']}}" alt="{{$image['name']}}">
								@endforeach
							</div>    		
				    	</div>				      
                    </div>
				  </div>
				</div>				
            </div>
            <div class="span6">		    
		    		<div align="center" style="min-height: 32px;">
		    			<div class="clearfix">
							<div class="btn-group inline pull-left" id="texteditor" style="display:none">			
								<button id="font-family" style="background-color: white;border-style: solid;border-color: black;" class="btn dropdown-toggle" data-toggle="dropdown" title="Font Style"><i class="glyphicon glyphicon-font"></i></button>		                    
							    <ul class="dropdown-menu" role="menu" aria-labelledby="font-family-X">
								    <li><a tabindex="-1" href="#" onclick="setFont('Arial');" class="Arial">Arial</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Helvetica');" class="Helvetica">Helvetica</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Myriad Pro');" class="MyriadPro">Myriad Pro</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Delicious');" class="Delicious">Delicious</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Verdana');" class="Verdana">Verdana</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Georgia');" class="Georgia">Georgia</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Courier');" class="Courier">Courier</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Comic Sans MS');" class="ComicSansMS">Comic Sans MS</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Impact');" class="Impact">Impact</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Monaco');" class="Monaco">Monaco</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Optima');" class="Optima">Optima</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Hoefler Text');" class="Hoefler Text">Hoefler Text</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Plaster');" class="Plaster">Plaster</a></li>
								    <li><a tabindex="-1" href="#" onclick="setFont('Engagement');" class="Engagement">Engagement</a></li>
				                </ul>
							    <button id="text-bold" style="background-color: white;border-style: solid;border-color: black" class="btn" data-original-title="Bold"><i class="glyphicon glyphicon-bold" style="width:19px;height:19px;"></i></button>
							    <button id="text-italic" style="background-color: white;border-style: solid;border-color: black" class="btn" data-original-title="Italic"><i class="glyphicon glyphicon-italic" style="width:19px;height:19px;"></i></button>
							    <button id="text-strike" style="background-color: white;border-style: solid;border-color: black" class="btn" title="Strike" style=""><i class="fas fa-strikethrough"></i></button>
							 	<button id="text-underline" style="background-color: white;border-style: solid;border-color: black" class="btn" title="Underline" style=""><i class="fas fa-underline"></i></button>
							 	<a class="btn" href="#" rel="tooltip" data-placement="top" data-original-title="Font Color" style="width:45px;height:34px;border-style: solid;border-color: black"><input type="hidden" id="text-fontcolor" class="color-picker" size="7" value="#000000"></a>
						 		<a class="btn" href="#" rel="tooltip" data-placement="top" data-original-title="Font Border Color" style="width:45px;height:34px;border-style: solid;border-color: black"><input type="hidden" id="text-strokecolor" class="color-picker" size="7" value="#000000"></a>
								  <!--- Background <input type="hidden" id="text-bgcolor" class="color-picker" size="7" value="#ffffff"> --->
							</div>							  
							<div class="pull-right" align="" id="imageeditor" style="display:none">
							  <div class="btn-group">										      
							      <button class="btn" id="bring-to-front" title="Bring to Front"><i class="glyphicon glyphicon-fast-backward rotate" style="height:19px;"></i></button>
							      <button class="btn" id="send-to-back" title="Send to Back"><i class="glyphicon glyphicon-fast-forward rotate" style="height:19px;"></i></button>
							      <button id="flip" type="button" class="btn" title="Show Back View"><i class="	glyphicon glyphicon-refresh" style="height:19px;"></i></button>
							      <button id="remove-selected" class="btn" title="Delete selected item"><i class="glyphicon glyphicon-trash" style="height:19px;"></i></button>
							  </div>
							</div>			  
							<div class="pull-right" align="" id="imageeditor" style="display:none">
							  <div class="btn-group">										      
							      <button class="btn" id="bring-to-front" title="Bring to Front"><i class="glyphicon glyphicon-fast-backward rotate" style="height:19px;"></i></button>
							      <button class="btn" id="send-to-back" title="Send to Back"><i class="glyphicon glyphicon-fast-forward rotate" style="height:19px;"></i></button>
							      <button id="flip" type="button" class="btn" title="Show Back View"><i class="	glyphicon glyphicon-refresh" style="height:19px;"></i></button>
							      <button id="remove-selected" class="btn" title="Delete selected item"><i class="glyphicon glyphicon-trash" style="height:19px;"></i></button>
							  </div>
							</div>			  
						</div>												
					</div>									  		
				<!--	EDITOR      -->					
					<div id="shirtDiv" class="page" style="width: 530px; height: 630px; position: relative; background-color: rgb(255, 255, 255);">
						
						<img id="tshirtFacing" crossorigin="anonymous"src="{{asset('img/crew_front.png')}}"></img>
						<div id="drawingArea" style="position: absolute;top: 100px;left:160px;z-index: 10;width: 200px;height: 400px;">
						<canvas id="tcanvas" width="200" height="400" class="hover" style="-webkit-user-select: none;"></canvas>		
						</div>
					</div>
                    <center><a id="saveImg" class="btn btn-info btn-lg" style="position:relative;">
                            <span class="fa fa-download"></span> Save</a></center>
            </div>
            
            <div class="span3">
                <div class="well">
                    <h3>Thanh toán</h3>
                            <p>
                                <table class="table">
                                    <tr>
                                        <td>Giá áo</td>
                                        <td align="right">50,000đ</td>
                                    </tr>
                                    <tr>
                                        <td>Tiền in</td>
                                        <td align="right">50,000đ</td>
                                    </tr>
									<tr>
										<td>Số lượng: </td>
										<td align="right"><input type="number" id="soluong" name="soluong" min="1" style="width:50px;" value="1";></td>
									</tr>
                                    <tr>
                                        <td><strong>Tổng tiền</strong></td>
                                        <td align="right"><p id="tongtien">100,000đ</p></td>
                                    </tr>
                                </table>			
                            </p>
                              <button type="button" class="btn btn-large btn-block btn-success" name="addToTheBag" id="addToTheBag">Thêm vào giỏ <i class="icon-briefcase icon-white"></i></button>
                        </div>
            </div>
            
        </div>
</div>
@endsection