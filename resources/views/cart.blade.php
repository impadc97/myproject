@extends('layouts.lib')
@section('content')

<h2>Giỏ hàng</h2>
     <div class="simpleCart_items">
     </div>
     <h3>
         Tổng tiền: <span class="simpleCart_grandTotal" name="amount"></span>
         <span><button class="btn btn-primary pull-right" data-toggle="modal" data-target="#order">Đặt hàng</button></span>
     </h3>
     <div id="order" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Đặt hàng</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body" style="display:block">
                    <form role="form" id="orderForm" method="POST" action="" >
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="font-weight:bold" for="tennguoinhan">Tên người nhận:</label>
                            <input class="col-sm-8" type="text" id="tennguoinhan" name="tennguoinhan">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"style="font-weight:bold"for="diachi">Địa chỉ người nhận:</label>
                            <input class="col-sm-8" type="text"  id="diachi"name="diachi">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" style="font-weight:bold"for="sdt">SĐT người nhận:</label>
                            <input class="col-sm-8"type="text" id="sdt" name="sdt">
                        </div>
                    </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Huỷ</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnOrder">Đặt hàng</button> 
                </div>
            </div>

        </div>
    </div>
@endsection