<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Design T-Shirt</title>

    
    <!-- Styles -->
	<link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jquery-minicolors/2.2.6/jquery.minicolors.css" rel="stylesheet">
    <link href="{{asset('css/bootstrap-responsive.min.css')}}" rel="stylesheet">
	<link re="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <style type="text/css">
		 .footer {
			padding: 70px 0;
			margin-top: 70px;
			border-top: 1px solid #E5E5E5;
			background-color: whiteSmoke;
			}
	      .color-preview {
	      	border: 1px solid #CCC;
	      	margin: 2px;
	      	zoom: 1;
	      	vertical-align: top;
	      	display: inline-block;
	      	cursor: pointer;
	      	overflow: hidden;
	      	width: 20px;
	      	height: 20px;
	      }
	      .rotate {  
		    -webkit-transform:rotate(90deg);
		    -moz-transform:rotate(90deg);
		    -o-transform:rotate(90deg);
		    /* filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=1.5); */
		    -ms-transform:rotate(90deg);		   
		}		
		.Arial{font-family:"Arial";}
		.Helvetica{font-family:"Helvetica";}
		.MyriadPro{font-family:"Myriad Pro";}
		.Delicious{font-family:"Delicious";}
		.Verdana{font-family:"Verdana";}
		.Georgia{font-family:"Georgia";}
		.Courier{font-family:"Courier";}
		.ComicSansMS{font-family:"Comic Sans MS";}
		.Impact{font-family:"Impact";}
		.Monaco{font-family:"Monaco";}
		.Optima{font-family:"Optima";}
		.HoeflerText{font-family:"Hoefler Text";}
		.Plaster{font-family:"Plaster";}
		.Engagement{font-family:"Engagement";}
		@media (min-width: 576px){
			.navbar-expand-sm .navbar-nav {
				-ms-flex-direction: row;
				flex-direction: row;
			}
		}
		.navbar-nav {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-direction: column;
			flex-direction: column;
			padding-left: 0;
			margin-bottom: 0;
			list-style: none;
			color: #007bff !important;
		}
		@media (min-width: 576px){
			.navbar-expand-sm .navbar-nav .nav-link {
				padding-right: .5rem;
				padding-left: .5rem;
			}
		}
		.nav-link {
			display: block;
			padding: .5rem 1rem;
		}
		a {
			color: #007bff !important;
			text-decoration: none;
			background-color: transparent;
			-webkit-text-decoration-skip: objects;
		}
		#uploadImg{
			color:black!important;
		}
		#saveImg{
			color:black!important;
		}
    </style>
	
</head>
<body>
			@if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">{{Auth::user()->tenuser}}</a>
                        <a href="{{url('/cart')}}" class="fa fa-shopping-cart fa-3x"><span class="badge badge-primary simpleCart_quantity"></span></a>
                    @else
                        <a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in">Login</a>
                        <a href="{{ route('register') }}"><span class="glyphicon glyphicon-user">Register</a>
                    @endauth
                </div>
            @endif
            <nav class="navbar navbar-expand-sm bg-light navbar-default">
                <!-- Brand/logo -->
                <a class="navbar-brand" href="{{ url('/') }}">Logo</a>
                
                <!-- Links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                    <a class="nav-link" href="/">Sản phẩm</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/customshirt">Thiết kế</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Liên hệ</a>
                    </li>
                </ul>
            </nav>
@yield('content')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-minicolors/2.2.6/jquery.minicolors.js"></script>
    <script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.3.3/fabric.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{asset('js/tshirtEditor.js')}}"></script>
	<script src="{{asset('js/simpleCart.js')}}" type="text/javascript"></script>
</body>
</html>