<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .full-height {
                height: 100vh;
            }

            .banner-top {
                background-color: #fff;
                margin-top: -5px;
                padding: 10px 0;
            }

            .banner-top .banner-thumb {
                padding: 10px 10px 10px 0;
                height: 120px;
                background-color: #fff;
                transition: all 0.4s;
            }

            .banner-top .banner-thumb .thumb {
                position: relative;
            }

            .banner-top .banner-thumb .thumb:after {
                position: absolute;
                content: "";
                width: 0;
                height: 0;
                border-style: solid;
                border-width: 10px 0 10px 10px;
                border-color: transparent transparent transparent #32c5d2;
                right: -8px;
                top: 20px;
                -webkit-transition: all 0.4s ease 0s;
                -moz-transition: all 0.4s ease 0s;
                -o-transition: all 0.4s ease 0s;
                transition: all 0.4s ease 0s;
            }

            .banner-top .banner-thumb i {
                max-width: 100px;
                margin-left: 22px;
                box-shadow: 0 5px 6px 0 rgba(0, 0, 0, 0.1);
                color: #32c5d2;
                float: left;
                border: 1px solid #32c5d2;
                width: 70px;
                height: 70px;
                line-height: 70px;
                text-align: center;
                margin-bottom: 20px;
                -webkit-transition: all 0.4s ease 0s;
                -moz-transition: all 0.4s ease 0s;
                -o-transition: all 0.4s ease 0s;
                transition: all 0.4s ease 0s;
            }

            .banner-top .banner-thumb:hover i {
                color: #fff;
                background-color: #46b8da;
            }

            .banner-top .banner-thumb h6 {
                padding-top: 10px;
                color: #434a54;
            }
            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                z-index:1;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .carousel-inner{
                margin-top:56px;
            }
            .carousel-inner img {
                width: 100%;
                height: 100%;
            }
            @media (min-width: 576px)
            {
                .navbar-expand-sm {
                position: absolute;
                top:0;
                left:0;
                width: 100%;
                }
            }
            #map{
                width:800px;
                height:600px;
                margin-top:10px;
            }
            .myDiv{
                margin-top:56px;
            }
            .h1 small {
                font-size: 24px;
            }
        </style>
</head>
<body>
        @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">{{Auth::user()->tenuser}}</a>
                        <a href="{{url('/cart')}}" class="fa fa-shopping-cart fa-3x"><span class="badge badge-primary simpleCart_quantity"></span></a>
                    @else
                        <a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in">Login</a>
                        <a href="{{ route('register') }}"><span class="glyphicon glyphicon-user">Register</a>
                    @endauth
                </div>
            @endif
            <nav class="navbar navbar-expand-sm bg-light navbar-default">
                <!-- Brand/logo -->
                <a class="navbar-brand" href="{{ url('/') }}">Logo</a>
                
                <!-- Links -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                    <a class="nav-link" href="/">Sản phẩm</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="/customshirt">Thiết kế</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="#">Liên hệ</a>
                    </li>
                </ul>
            </nav>
        @yield('content')

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
     <script src="{{asset('js/simpleCart.js')}}" type="text/javascript"></script>
     <script src="{{asset('js/script.js')}}"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".dropdown-toggle").dropdown();
        });
    </script>
</body>
</html>
