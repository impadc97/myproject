@extends('layouts.admin')
@section('content')
<div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 gutter">
                            <table id="ordertable" class="easyui-datagrid" url="/admin/ordersjson"
                                rownumbers="true" pagination="true" method="get" fitColumns="true">
                                    
                                    <thead>
                                    <tr>
                                    <th field="id" width="150">Mã đơn hàng</th>
                                    <th field="user_id" width="150">User ID</th>
                                    <th field="tongtien" width="150">Giá tiền</th>
                                    <th field="tennguoinhan" width="200">Họ tên người nhận</th>
                                    <th field="sdt" width="150">SĐT người nhận</th>
                                    <th field="diachi" width="200">Địa chỉ giao hàng</th>
                                </tr>
                                </thead>
                                <tbody>
                                 @foreach ($allOrders['data'] as $Orders)
                                <tr>
                                    <td class="orderID">{{$Orders['id']}}</td>
                                    <td>{{$Orders['user_id']}}</td>
                                    <td>{{$Orders['tongtien']}}</td>
                                    <td>{{$Orders['tennguoinhan']}}</td>
                                    <td>{{$Orders['sdt']}}</td>
                                    <td>{{$Orders['diachi']}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                    </tfoot>
                                </table>
                            <button class="btn btn-primary pull-right" id="btnDelOr" style="margin-top:10px">Xoá</button>
                        </div>
</div>

@endsection