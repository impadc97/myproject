<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ProductsController@index' )->name('Products');
Route::get('/home/changepwd',function(){
    return view('changePwd');
});
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cart',function(){
    return view('cart');
})->name('cart');
Route::post('/register','RegisterController@create');
Route::post('/contact','UsersController@contact')->name('contact');
Route::put('/home','UsersController@update')->name('editUser');
Route::put('/home/changePwd','UsersController@changePwd')->name('changePwd');
Route::post('/cart','OrdersController@store')->name('order');
Route::get('/home/history','OrdersController@history')->name('history');
Route::get('/home/history/{id}','OrdersController@show')->name('detail');
Route::get('/customshirt','LibImgController@index')->name('design');
Route::post('/products','ProductsController@storeDes');
Route::get('/products/{id}','ProductsController@show');
Route::get('/contact',function(){
    return view('contact');
});
Route::prefix('admin')->group(function(){
    Route::get('orders','OrdersController@index')->name('listorder');
    Route::get('users','UsersController@index')->name('listuser');
    Route::get('products','ProductsController@list')->name('listprod');
    Route::get('productsjson','ProductsController@json')->name('prodjson');
    Route::get('ordersjson','OrdersController@json')->name('orderjson');
    Route::post('ordersjson/delete','OrdersController@destroy')->name('delorder');
    Route::post('ordersjson/update','OrdersController@update')->name('editorder');
    Route::get('usersjson','UsersController@json')->name('userjson');
    Route::post('productsjson/add','ProductsController@store')->name('createProd');
    Route::post('productsjson/edit/{id}','ProductsController@edit')->name('editProd');
    Route::post('productsjson/del','ProductsController@destroy')->name('delProd');
    Route::post('usersjson/ban','UsersController@ban')->name('banUser');
    Route::post('usersjson/del','UserSController@destroy')->name('delUser');
});
Auth::routes();


